<?php

use DKM\EventNewsPlugin\Hooks\ZZZFlexFormHook;
use DKM\EventNewsPlugin\Hooks\OverrideSettings;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

defined('TYPO3') || die();

$boot = function () {
    /**
     * Override default cachecontrol setup
     */
    $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['cachecontrol']['clearCacheConfig'] = [
        'sys_category' => [
            ['extension' => 'eventnews'],
            ['extension' => 'news']
        ],
        'tt_content' => [
            ['extension' => 'news', 'condition' => ['field' => 'CType', 'comparisonType' => 'inArray', 'value' => ['news_pi1','news_categorylist','news_newsdatemenu','news_newsdetail','news_newsliststicky','news_newssearchform','news_newssearchresult','news_newsselectedlist','eventnewsplugin_pi3']]],
            ['extension' => 'eventnews', 'condition' => ['field' => 'CType', 'comparisonType' => 'inArray', 'value' => ['eventnewsplugin_pi1','eventnewsplugin_pi2','eventnews_newsmonth']]]
        ],
        'tx_news_domain_model_news' => [
            ['extension' => 'eventnews', 'condition' => ['field' => 'is_event', 'value' => 1]],
            ['extension' => 'news', 'condition' => ['field' => 'is_event', 'value' => 0]],
        ]
    ];

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Eventnewsplugin',
        'Pi1',
        [
            \GeorgRinger\Eventnewsplugin\Controller\NewsController::class => 'month',
            \GeorgRinger\Eventnewsplugin\Controller\CategoryController::class => 'list',
            \GeorgRinger\Eventnewsplugin\Controller\TagController::class => 'list',
        ],
        [
            \GeorgRinger\Eventnewsplugin\Controller\NewsController::class => 'searchForm,searchResult',
        ],
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
    );
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Eventnewsplugin',
        'Pi2',
        [
            \GeorgRinger\Eventnews\Controller\NewsController::class => 'selectedList',
        ],
        [],
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
    );
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Eventnewsplugin',
        'Pi3',
        [
            \GeorgRinger\News\Controller\NewsController::class => 'selectedList',
        ],
        [],
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
    );
    $extCfg = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('eventnewsplugin');

    ExtensionManagementUtility::addTypoScript('Eventnewsplugin','setup',"
    
    
    
plugin.tx_news.settings.useItemsPerPageIfLimitIsEmpty = 1
# Nyhedsliste
tt_content.news_pi1.20 {
    settings {
        contentType = news
        contentSubType = list
        useStdWrap = startingpoint
        startingpoint.ifEmpty = {\${$extCfg['newsStartingPointConstant']}}
        defaultDetailPid = {\${$extCfg['newsDefaultDetailPidConstant']}}
    }
}

# Begivenhedsliste
tt_content.eventnewsplugin_pi1.20 {
    settings {
        contentType = events
        contentSubType = list
        useStdWrap = timeRestriction,startingpoint,orderDirection
        timeRestriction.ifEmpty = now
        startingpoint.ifEmpty = {\${$extCfg['eventnewsStartingPointConstant']}}
        defaultDetailPid = {\${$extCfg['eventnewsDefaultDetailPidConstant']}}
        orderDirection.override = asc
        demandClass = GeorgRinger\Eventnews\Domain\Model\Dto\Demand
        eventRestriction = 1
    }
    extensionName = News
}

# Begivenheder (udvalgte)
tt_content.eventnewsplugin_pi2 < tt_content.news_newsselectedlist
tt_content.eventnewsplugin_pi2.20.settings < tt_content.eventnewsplugin_pi1.20.settings
tt_content.eventnewsplugin_pi2 {
  20.settings {
    contentSubType = selectedList
    orderBy =
    orderDirection = 
  }
}

# Nyheder (udvalgte)
tt_content.eventnewsplugin_pi3 < tt_content.news_newsselectedlist
tt_content.eventnewsplugin_pi3.20.settings < tt_content.news_pi1.20.settings
tt_content.eventnewsplugin_pi3 {
  20.settings {
    contentSubType = selectedList
    orderBy =
    orderDirection = 
  }
}

    ", 'defaultContentRendering');

    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
        \TYPO3\CMS\Core\Imaging\IconRegistry::class
    );
    $iconRegistry->registerIcon(
        'plugin-cal',
        \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
        ['source' => 'EXT:eventnewsplugin/Resources/Public/Images/eventnews_plugin.gif']
    );
    $iconRegistry->registerIcon(
        'apps-pagetree-folder-contains-board',
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:core/Resources/Public/Icons/T3Icons/svgs/apps/apps-pagetree-folder-contains-board.svg']
//        ['source' => 'EXT:eventnewsplugin/Resources/Public/Icons/ext-eventnews-folder-tree.svg']
    );

    // Update flexforms
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['routing']['aspects']['EventNewsMapper'] = \DKM\EventNewsPlugin\Routing\Aspect\EventNewsMapper::class;

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']['eventnewsplugin'] =
        \DKM\EventNewsPlugin\Hooks\Backend\EventNewsDataHandlerHook::class;

    // Overwrite page module hook definition
//    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['list_type_Info']['news_pi1']['news'] =
//        \DKM\EventNewsPlugin\Hooks\PageLayoutView::class . '->getExtensionSummary';
//    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['list_type_Info']['eventnewsplugin_pi1']['news'] =
//        \DKM\EventNewsPlugin\Hooks\PageLayoutView::class . '->getExtensionSummary';

    $GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['Controller/NewsController.php']['overrideSettings'][] = OverrideSettings::class . '->main';
};

$boot();
unset($boot);
