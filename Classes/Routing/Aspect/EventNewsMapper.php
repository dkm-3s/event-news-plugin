<?php


namespace DKM\EventNewsPlugin\Routing\Aspect;


use TYPO3\CMS\Core\Routing\Aspect\PersistedPatternMapper;
use TYPO3\CMS\Core\Site\SiteLanguageAwareTrait;

/**
 * Example configuration with format {day}-{month}-{year}-{path_segment}-{uid}
 * Should be resistant to brute-force scenarios and the risk of cache-flooding
EventNewsPlugin:
  limitToPages: ['%settings.pids.news.detail%','%settings.pids.events.detail%']
  type: Extbase
  extension: News
  plugin: Pi1
  routes:
    - routePath: '/{news_title}'
      _controller: 'News::detail'
      _arguments:
      news_title: news
  defaultController: 'News::detail'
  aspects:
    news_title:
      type: EventNewsMapper
      tableName: 'tx_news_domain_model_news'
      routeFieldPattern: '^(?P<day>\d+)-(?P<month>\d+)-(?P<year>\d+)-(?P<path_segment>.+)-(?P<uid>\d+)$'
      routeFieldResult: '{day}-{month}-{year}-{path_segment}-{uid}'
 *
 * Class EventNewsMapper
 * @package DKM\EventNewsPlugin\Routing\Aspect
 */
class EventNewsMapper extends PersistedPatternMapper implements \TYPO3\CMS\Core\Site\SiteLanguageAwareInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve(string $value): ?string
    {
        if (!preg_match('#' . $this->routeFieldPattern . '#', $value, $matches)) {
            return null;
        }
        $values = $this->filterNamesKeys($matches);
        $dateValues = [];
        //when year, month or day then move from $values to $dateValues
        array_walk($values, function($val,$key) use(&$values, &$dateValues){
            if (in_array($key, ['day','month','year'])) {
                $dateValues[$key] = $values[$key];
                unset($values[$key]);
            }
        });
        //@todo not sure if this the correct substitution
//        $result = $this->getPersistenceDelegate()->resolve($values);
        $result = $this->findByRouteFieldValues($values);
        # Prevent news and events from other sites
        $configuration = $this->getSite()->getConfiguration();
        if (empty($result['pid'])) {
            return null;
        }
        if (\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\Features::class)->isFeatureEnabled('NewsletterFeature2024')) {
            if($result['pid'] != ($configuration['settings']['pids']['news']['storage'] ?? null) && $result['pid'] != ($configuration['settings']['pids']['events']['storage'] ?? null)) {
                return null;
            }
        } elseif ($result['pid'] != ($configuration['3S']['pids']['news']['storage'] ?? null) && $result['pid'] != ($configuration['3S']['pids']['events']['storage'] ?? null)) {
            return null;
        }
        //@todo2 now test if the values fo $dateValues matches values of the datetime field
        foreach ($dateValues as $fieldName => $dateValue) {
            $dateFormat = $this->getDateFormat($fieldName);
            if(date($dateFormat, $result['datetime']) != $dateValue) {
                return null;
            }
        }

        if ($result[$this->languageParentFieldName] ?? null > 0) {
            return (string)$result[$this->languageParentFieldName];
        }
        if (isset($result['uid'])) {
            return (string)$result['uid'];
        }
        return null;
    }

    protected function getDateFormat($fieldName) {
        switch ($fieldName) {
            case 'year':
                $dateFormat = ($this->settings['dateFormat']['year'] ?? 'Y') ?: 'Y';
                break;
            case 'month':
                $dateFormat = ($this->settings['dateFormat']['month'] ?? 'n') ?: 'n';
                break;
            case 'day':
                $dateFormat = ($this->settings['dateFormat']['day'] ?? 'j') ?: 'j';
                break;
            default:
                $dateFormat = null;
        }
        return $dateFormat;
    }

    /**
     * @throws \InvalidArgumentException
     */
    protected function createRouteResult(?array $result): ?string
    {
        if ($result === null) {
            return $result;
        }
        $substitutes = [];
        foreach ($this->routeFieldResultNames as $fieldName) {
            $dateFormat = $this->getDateFormat($fieldName);
            if (!isset($result[$fieldName]) && !$dateFormat) {
                return null;
            }
            $routeFieldName = '{' . $fieldName . '}';
            $substitutes[$routeFieldName] = $dateFormat ? date($dateFormat, $result['datetime']) : $result[$fieldName];
        }
        return str_replace(
            array_keys($substitutes),
            array_values($substitutes),
            $this->routeFieldResult
        );
    }
//    protected \TYPO3\CMS\Core\Site\Entity\SiteLanguage $siteLanguage;


}