<?php
namespace DKM\EventNewsPlugin\Hooks;

use TYPO3\CMS\Core\Configuration\Features;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class OverrideSettings
 * @package DKM\EventNewsPlugin\Hooks
 */
class OverrideSettings
{
    protected $originalSettings;
    protected $tsSettings;

    /**
     * @param $params
     * @param $_
     * @return mixed
     */
    public function main($params, $_) {
        $this->originalSettings = &$params['originalSettings'];
        $this->tsSettings = &$params['tsSettings'];

        $layoutKey = $this->originalSettings['listLayout'] = $this->getLayoutKey();
        switch ($layoutKey) {
            case 'Standard':
                break;
            case 'WithImage':
            case 'InRow':
                $this->originalSettings['media'] = $this->tsSettings['settings']['media'] = $this->tsSettings['settings']['listAlt']['media'];
                $this->tsSettings['settings']['list']['paginate']['itemsPerPage'] = $this->tsSettings['settings']['listAlt']['paginate']['itemsPerPage'];
                break;
            default:
        }
        if(!$this->isItemsPerPage()) {
            $this->originalSettings['list']['paginate']['itemsPerPage'] = $this->tsSettings['settings']['list']['paginate']['itemsPerPage'];
        }
        if($this->isHidePagination() && !$this->isLimit() &&
            ($this->tsSettings['settings']['useItemsPerPageIfLimitIsEmpty'] ?? false) &&
            ($this->tsSettings['settings']['list']['paginate']['itemsPerPage'] ?? false)) {
            $this->originalSettings['limit'] = $this->tsSettings['settings']['list']['paginate']['itemsPerPage'];
        }
        return $this->originalSettings;
    }

    /**
     * @param $type
     */
    protected function isContentType($type): bool
    {
        return ($this->originalSettings['contentType'] ?? false) && $this->originalSettings['contentType'] == $type;
    }

    protected function isHidePagination(): bool
    {
        return isset($this->originalSettings['hidePagination']) && $this->originalSettings['hidePagination'];
    }

    protected function isLimit(): bool
    {
        return isset($this->originalSettings['limit']) && $this->originalSettings['limit'];
    }

    protected function isItemsPerPage(): bool
    {
        return isset($this->originalSettings['list']['paginate']['itemsPerPage']) && $this->originalSettings['list']['paginate']['itemsPerPage'];
    }

    protected function isAlternativeListView(): bool
    {
        return (bool) (($this->originalSettings['settings.listLayout'] ?? '') == '' ? $this->originalSettings['fastinfo.listLayout'] : $this->originalSettings['settings.listLayout']);
    }

    /**
     * @return mixed|string
     */
    protected function getLayoutKey() {
        $layoutKey = ((trim($this->originalSettings['listLayout'] ?? '')) === '' || $this->originalSettings['listLayout'] === null) ? ($this->originalSettings['fastinfo']['listLayout'] ?? '') : $this->originalSettings['listLayout'];
        switch (trim((string)$layoutKey)) {
            case "1":
                $layoutKey = $this->isContentType('events') ? 'WithImage' : 'InRow';
                break;
            case "0":
            case "":
                $layoutKey = 'Standard';
                break;
            default:
        }
        return $layoutKey;
    }
}