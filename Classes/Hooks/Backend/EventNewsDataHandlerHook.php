<?php

namespace DKM\EventNewsPlugin\Hooks\Backend;

/**
 * This file is part of the "eventnews" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Messaging\AbstractMessage;

class EventNewsDataHandlerHook
{

    /**
     * Save a new news record as event if stated by tsconfig
     *
     * @param string $status status
     * @param string $table table name
     * @param int $recordUid id of the record
     * @param array $fields fieldArray
     * @param \TYPO3\CMS\Core\DataHandling\DataHandler $parentObject parent Object
     * @return void
     */
    public function processDatamap_postProcessFieldArray(
        $status,
        $table,
        $recordUid,
        array &$fields,
        \TYPO3\CMS\Core\DataHandling\DataHandler $parentObject
    ) {
        $event = array_merge($parentObject->checkValue_currentRecord ?: [], $fields ?: []);
        if ($table === 'tx_news_domain_model_news' && ($event['is_event'] ?? false)) {
            $datetime = $event['datetime'];
            $event_end = $event['event_end'];
            if($event['event_end'] && $datetime > $event_end) {
                $fields['event_end']  = $event['datetime'] + 3600;
                $message = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Messaging\FlashMessage::class,
                    'Tidspunktet i Begivenhedsslut må ikke ligge før tidspunktet i Dato og tid. Begivenhedsslut er blevet tilrettet med værdien fra Dato og tid tillagt 1 time',
                    'Værdi blev tilrettet', // [optional] the header
                    \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::WARNING, // [optional] the severity defaults to \TYPO3\CMS\Core\Messaging\FlashMessage::OK
                    true // [optional] whether the message should be stored in the session or only in the \TYPO3\CMS\Core\Messaging\FlashMessageQueue object (default is false)
                );
                $flashMessageService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Messaging\FlashMessageService::class);
                $messageQueue = $flashMessageService->getMessageQueueByIdentifier();
                $messageQueue->addMessage($message);
            }
        }
    }
}