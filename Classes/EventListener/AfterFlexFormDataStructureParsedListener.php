<?php

declare(strict_types=1);

namespace DKM\EventNewsPlugin\EventListener;

use Doctrine\DBAL\Exception;
use TYPO3\CMS\Core\Configuration\Event\AfterFlexFormDataStructureParsedEvent;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;

class AfterFlexFormDataStructureParsedListener
{
    /**
     * GET CORRECT CATEGORIES FOR CATEGORY TREE
     *
     * @param AfterFlexFormDataStructureParsedEvent $event
     * @return void
     * @throws Exception
     * @throws ExtensionConfigurationExtensionNotConfiguredException
     * @throws ExtensionConfigurationPathDoesNotExistException
     * @throws SiteNotFoundException
     */
    public function __invoke(AfterFlexFormDataStructureParsedEvent $event): void
    {
        $dataStructure = $event->getDataStructure();
        $identifier = $event->getIdentifier();

        if ($identifier['type'] === 'tca' && $identifier['tableName'] === 'tt_content' && $identifier['dataStructureKey']) {
            if (!($GLOBALS['TYPO3_REQUEST'] ?? false)) {
                return;
            }
            $queryParams = $GLOBALS['TYPO3_REQUEST']->getQueryParams();
            if($GLOBALS['TYPO3_REQUEST']->getAttribute('route') && $GLOBALS['TYPO3_REQUEST']->getAttribute('route')->getPath() == '/ajax/record/tree/fetchData') {
                $pageId = null;
                $type = 0;
                switch ($identifier['dataStructureKey']) {
                    case 'eventnewsplugin_pi1,list':
                    case '*,eventnewsplugin_pi1':
                        $sysFolderUid = $this->getSysFolderUid('eventnewsplugin');
                        break;
//                    case 'news_pi1,list':
                    case '*,news_pi1':
                        $sysFolderUid = $this->getSysFolderUid('news');
                        break;
                }
                if(isset($sysFolderUid) && $sysFolderUid) {
                    $foreignTableWhere = &$dataStructure['sheets']['sDEF']['ROOT']['el']['settings.categories']['config']['foreign_table_where'];
                    $foreignTableWhere = " AND sys_category.pid = {$sysFolderUid} {$foreignTableWhere}";
                }
            }
        }
        $event->setDataStructure($dataStructure);
    }

    /**
     * @param $extension
     * @throws ExtensionConfigurationExtensionNotConfiguredException
     * @throws ExtensionConfigurationPathDoesNotExistException
     * @throws SiteNotFoundException|Exception
     */
    private function getSysFolderUid($extension)
    {
        $settingsSource = $this->getSettingsSource();
        $pageId = $this->getPageIdFromRequest();
        switch ($settingsSource) {
            case 'sitecfg constants':
            case 'sitecfg':
                $pageId = $this->getPageIdFromRequest();

                /** get site configuration from pageId - methods exists for this - @var Site $site */
                $site = GeneralUtility::makeInstance(SiteFinder::class)->getSiteByPageId((int)$pageId);

                // get value from site configuration with key bane
                $siteConfiguration = $site->getConfiguration();
                $key = $extension == 'eventnewsplugin' ? 'events' : 'news';
                if(\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\Features::class)->isFeatureEnabled('NewsletterFeature2024')) {
                    $storage = $siteConfiguration['settings']['pids'][$key]['storage'] ?? null;
                } else {
                    $storage = $siteConfiguration['3S']['pids'][$key]['storage'] ?? null;
                }

                if ($storage || $settingsSource == 'sitecfg') {
                    return $storage;
                }
            case 'constants':
                if ($pageId) {
                    return (int)$this->getSysFolderUidFromConstants($pageId, "tx_{$extension}");
                }
        }
        return null;
    }

    /**
     * @return false|mixed
     * @throws Exception
     */
    private function getPageIdFromRequest(): mixed
    {
        if (!$GLOBALS['TYPO3_REQUEST']) {
            return false;
        }
        if(($queryParams = $GLOBALS['TYPO3_REQUEST']->getQueryParams()) && (isset($queryParams['tableName']) && $queryParams['tableName'] == 'tt_content' && isset($queryParams['uid'])) && $queryParams['tableName'] == 'tt_content') {
            if($queryParams['command'] == 'edit') {
                return $this->getPageUidFromContentUid($queryParams['uid']);
            } elseif ($queryParams['command'] == 'new') {
                if($queryParams['uid'] < 0) {
                    return $this->getPageUidFromContentUid(abs((int)$queryParams['uid']));
                } else {
                    return $queryParams['uid'];
                }
            }
        };
        return false;
    }

    /**
     * @return mixed|string
     */
    private function getSettingsSource(): mixed
    {
        $settingsSource = 'constants';
        try {
            /** @var ExtensionConfiguration $extensionConfiguration */
            $extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class);
            $settingsSource = $extensionConfiguration->get('eventnewsplugin', 'settings_source');
        } catch (ExtensionConfigurationExtensionNotConfiguredException $exception) {
            //
        } catch (ExtensionConfigurationPathDoesNotExistException $exception) {
            //
        }
        return $settingsSource;
    }

    /**
     * @param $contentUid
     * @return false|mixed
     * @throws Exception
     * @deprecated should implement and use siteConfiguration instead (Site configuration not available)
     */
    private function getPageUidFromContentUid($contentUid): mixed
    {
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $qb = $connectionPool->getQueryBuilderForTable('tt_content');
        $qb->getRestrictions()->removeAll();
        return $qb->from('tt_content')
            ->select('pid')
        ->where($qb->expr()->eq('uid', $qb->createNamedParameter($contentUid)))->executeQuery()->fetchOne();
    }

    /**
     * @param $pageUid
     * @param $plugin
     * @return bool|mixed
     * @deprecated should implement and use siteConfiguration instead
     */
    function getSysFolderUidFromConstants($pageUid, $plugin): mixed
    {
        /** @var ExtendedTemplateService $typoScriptTemplate */
        $typoScriptTemplate = GeneralUtility::makeInstance(ExtendedTemplateService::class);
        $typoScriptTemplate->tt_track = 0; // Do not log time-performance information

        /** @var RootlineUtility $rootlineUtility */
        $rootlineUtility = GeneralUtility::makeInstance(RootlineUtility::class);
        $rootline = $rootlineUtility->get();

        $typoScriptTemplate->runThroughTemplates($rootline,0); // This generates the constants/config + hierarchy info for the template.
        $typoScriptTemplate->generateConfig_constants(); //file://Generate

        /** @var array $constants */
        switch ($plugin) {
            case 'tx_news':
                $constants = $typoScriptTemplate->substituteConstants($typoScriptTemplate->setup['constants']['plugin.']['tx_news.']['settings.']);
                break;
            case 'tx_eventnewsplugin':
                $constants = $typoScriptTemplate->substituteConstants($typoScriptTemplate->setup['constants']['plugin.']['tx_eventnewsplugin.']['settings.']);
                break;
        }
        if (isset($constants['startingpoint'])) {
            return $constants['startingpoint'];
        }
        return false;
    }
}