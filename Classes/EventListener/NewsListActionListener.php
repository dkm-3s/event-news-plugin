<?php

declare(strict_types=1);

namespace DKM\EventNewsPlugin\EventListener;

use GeorgRinger\News\Domain\Model\NewsDefault;
use GeorgRinger\News\Event\NewsListActionEvent;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Frontend\VariableFrontend;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class NewsListActionListener
{
    public function setLifeTime(NewsListActionEvent $event): void {
        $settings = $event->getAssignedValues()['settings'];
        if(isset($settings['contentType']) && $settings['contentType'] == 'events') {
            //Reset cached cache_timeout, otherwise the hook in TypoScriptFrontendController will never run
            /** @var  $runtimeCache VariableFrontend */
            $runtimeCache = GeneralUtility::makeInstance(CacheManager::class)->getCache('runtime');
            $cachedCacheLifetimeIdentifier = 'core-tslib_fe-get_cache_timeout';
            $runtimeCache->remove($cachedCacheLifetimeIdentifier);

            //find new cache lifetime
            /** @var NewsDefault $item */
            if($item = $event->getAssignedValues()['news']->getFirst()) {
                if($item->getEventEnd() && $item->getEventEnd() > $item->getDatetime()) {
                    $lifetimeSeconds = $item->getEventEnd()->getTimestamp() - \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Context\Context::class)->getPropertyFromAspect('date', 'timestamp');
                }
                if(!isset($lifetimeSeconds) || $lifetimeSeconds < 0) {
                    $lifetimeSeconds = $item->getDatetime()->getTimestamp() - \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Context\Context::class)->getPropertyFromAspect('date', 'timestamp');
                }
            }
            $lifetimeSeconds = $lifetimeSeconds ?? PHP_INT_MAX;
            //Pass new cache lifetime to closure function used for the hook in TypoScriptFrontendController
            $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['get_cache_timeout']['eventnewsplugin'] = function ($params, $that) use ($lifetimeSeconds) {
                unset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['get_cache_timeout']['eventnewsplugin']);
                return min($params['cacheTimeout'], $lifetimeSeconds);
            };
        }
    }
}