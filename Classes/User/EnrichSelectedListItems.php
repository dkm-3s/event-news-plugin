<?php

namespace DKM\EventNewsPlugin\User;

use Doctrine\DBAL\ArrayParameterType;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Enrich list of items (of events type) for the Selected List View with date
 */
class EnrichSelectedListItems
{

    /**
     * @param $params
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     */
    public function itemsProcFunc(&$params): void
    {
        if($params['flexParentDatabaseRow']['CType'] == 'eventnewsplugin_pi2') {
            foreach ($params['items'] as $item) {
                $uids[] = $item->getValue();
            }
            if($uids ?? false) {
                $table = 'tx_news_domain_model_news';
                $qb = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($table);
                $result = $qb->select('uid','datetime','title','uid')->from($table)->where($qb->expr()->or(
                    $qb->expr()->in('uid', $qb->createNamedParameter($uids, ArrayParameterType::INTEGER))
                ))->executeQuery()->fetchAllAssociativeIndexed();

                foreach ($params['items'] as &$item) {
                    $item[0] = "{$this->getDateTime($result[$item[1]]['datetime'])}: {$result[$item[1]]['title']}";
                }
            }
        }
    }

    /**>
     * @param $timestamp
     * @return string
     * @throws \Exception
     */
    private function getDateTime($timestamp): string
    {
        $date = new \DateTime('@' . $timestamp);
        $date->setTimezone(new \DateTimeZone(date_default_timezone_get()));
        return $date->format('d-m-Y');
    }
}