<?php
defined('TYPO3') || die();

// Override news icon
$GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
    0 => 'LLL:EXT:eventnewsplugin/Resources/Private/Language/locallang_be.xlf:eventnews-folder',
    1 => 'eventnewsplugin',
    2 => 'apps-pagetree-folder-contains-board'
];
$GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-eventnewsplugin'] = 'apps-pagetree-folder-contains-board';