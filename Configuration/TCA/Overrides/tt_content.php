<?php
defined('TYPO3') || die();

$pluginConfig = [

    // *** Plugin Event News ***
    'pi1' => [
        'flexformFile' => 'EXT:news/Configuration/FlexForms/flexform_news.xml',
        'group' => 'Kalender'
    ],

    // *** Plugin Event News - selected events ***
    'pi2' => [
        'flexformFile' => 'EXT:eventnewsplugin/Configuration/FlexForm/flexform_news_selected_v2.xml',
        'group' => 'Kalender'
    ],

    // *** Plugin News - selected news ***
    'pi3' => [
        'flexformFile' => 'EXT:eventnewsplugin/Configuration/FlexForm/flexform_news_selected_v2.xml',
        'group' => 'Nyheder'
    ]
];

foreach ($pluginConfig as $pluginName => $config) {
    $contentTypeName = 'eventnewsplugin_' . $pluginName;
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'eventnewsplugin',
        ucfirst($pluginName),
        "LLL:EXT:eventnewsplugin/Resources/Private/Language/locallang_be.xlf:{$pluginName}_title",
        'ext-news-plugin-news-list',
        $config['group']
    );
    $GLOBALS['TCA']['tt_content']['types'][$contentTypeName]['subtypes_excludelist'] = 'recursive,select_key,pages';
//    $GLOBALS['TCA']['tt_content']['types'][$contentTypeName]['subtypes_addlist'] = 'pi_flexform';
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        '*',
        "FILE:{$config['flexformFile']}",
        $contentTypeName
    );

    // Add the FlexForm to the show item list
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tt_content',
        '--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.plugin, pi_flexform',
        $contentTypeName,
        'after:palette:headers'
    );
}

// Rename group named news to Nyheder
foreach ($GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'] as &$item) {
    if($item['group'] == 'news') {
        $item['group'] = 'Nyheder';
    }
}

///*********************
// * Plugin Event News *
// ********************/
//$contentTypeName = 'eventnewsplugin_pi1';
//\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
//    'eventnewsplugin',
//    'Pi1',
//    'LLL:EXT:eventnewsplugin/Resources/Private/Language/locallang_be.xlf:pi1_title',
//    'ext-news-plugin-news-list',
//    'Kalender'
//);
//$GLOBALS['TCA']['tt_content']['types'][$contentTypeName]['subtypes_excludelist'] = 'recursive,select_key,pages';
//$GLOBALS['TCA']['tt_content']['types'][$contentTypeName]['subtypes_addlist'] = 'pi_flexform';
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
//    '*',
//    'FILE:EXT:news/Configuration/FlexForms/flexform_news.xml',
//    $contentTypeName
//);
//// Add the FlexForm to the show item list
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
//    'tt_content',
//    '--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.plugin, pi_flexform',
//    $contentTypeName,
//    'after:palette:headers'
//);
//
//
///***************************************
// * Plugin Event News - selected events *
// **************************************/
//$contentTypeName = 'eventnewsplugin_pi2';
//\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
//    'eventnewsplugin',
//    'Pi2',
//    'LLL:EXT:eventnewsplugin/Resources/Private/Language/locallang_be.xlf:pi2_title',
//    'ext-news-plugin-news-list',
//    'Kalender'
//);
//$GLOBALS['TCA']['tt_content']['types'][$contentTypeName]['subtypes_excludelist'] = 'recursive,select_key,pages';
//$GLOBALS['TCA']['tt_content']['types'][$contentTypeName]['subtypes_addlist'] = 'pi_flexform';
//
//// Add the FlexForm to the show item list
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
//    'tt_content',
//    '--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.plugin, pi_flexform',
//    $contentTypeName,
//    'after:palette:headers'
//);
//
//
//
//// Remember to delete Configuration/FlexForm/flexform_news_selected.xml when removing this Feature toggle
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
//    '*',
//    'FILE:EXT:eventnewsplugin/Configuration/FlexForm/flexform_news_selected_v2.xml',
//    $contentTypeName
//);
//
///*******************************
// * Plugin News - selected news *
// ******************************/
//\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
//    'eventnewsplugin',
//    'Pi3',
//    'LLL:EXT:eventnewsplugin/Resources/Private/Language/locallang_be.xlf:pi3_title',
//    'ext-news-plugin-news-list',
//    'Nyheder'
//);
//$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['eventnewsplugin_pi3'] = 'recursive,select_key,pages';
//$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['eventnewsplugin_pi3'] = 'pi_flexform';
//
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('*',
//    'FILE:EXT:eventnewsplugin/Configuration/FlexForm/flexform_news_selected_v2.xml',
//    'eventnewsplugin_pi3'
//);
//