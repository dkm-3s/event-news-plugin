mod.wizards.newContentElement.wizardItems.plugins.elements {
    calendar {
        iconIdentifier = plugin-cal
        title = Kalender modul
        description = Liste- og enkel-visning af kalender begivenheder
        tt_content_defValues {
            CType = eventnewsplugin_pi1
        }
    }
}