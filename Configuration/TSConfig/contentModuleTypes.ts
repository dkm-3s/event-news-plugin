TCEFORM.tx_news_domain_model_news.is_event.disabled = 1
mod.web_list.tableDisplayOrder.sys_category.after = tx_news_domain_model_news
[page && page["module"] == "eventnewsplugin"]
TCEFORM.sys_category.parent.disabled = 1
tx_news.newRecordAsEvent = 1
TCAdefaults.tx_news_domain_model_news.is_event = 1
mod.web_list {
    allowedNewTables = tx_news_domain_model_news,sys_category,tx_kabooimport_domain_model_importinfo,tx_eventnews_domain_model_location
    noCreateRecordsLink = 1
}
TCA.tx_news_domain_model_news.ctrl.title = LLL:EXT:eventnewsplugin/Resources/Private/Language/locallang_be.xlf:event
# Todo this can probably be removed
TCEMAIN.clearCacheCmd = sub(root)
[GLOBAL]
#Allow admin to create new records
[page && page["module"] == "eventnewsplugin" && backend.user.isAdmin]
mod.web_list.noCreateRecordsLink = 0
[GLOBAL]


[page && page["module"] == "news"]
TCEFORM.sys_category.parent.disabled = 1
mod.web_list {
    allowedNewTables = tx_news_domain_model_news,sys_category
    noCreateRecordsLink = 1
}
TCA.tx_news_domain_model_news.ctrl.title = LLL:EXT:eventnewsplugin/Resources/Private/Language/locallang_be.xlf:news
# Todo this can probably be removed
TCEMAIN.clearCacheCmd = sub(root)
[GLOBAL]
#Allow admin to create new records
[page && page["module"] == "news" && backend.user.isAdmin]
mod.web_list.noCreateRecordsLink = 0
[GLOBAL]
