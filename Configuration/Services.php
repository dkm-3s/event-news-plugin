<?php
declare(strict_types=1);

use DKM\EventNewsPlugin\Hooks\Backend\EventNewsDataHandlerHook;
use DKM\EventNewsPlugin\Hooks\ZZZFlexFormHook;
use DKM\EventNewsPlugin\Hooks\OverrideSettings;
//use DKM\EventNewsPlugin\Hooks\PageLayoutView;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use TYPO3\CMS\Core\DependencyInjection;

return function (ContainerConfigurator $container, ContainerBuilder $containerBuilder) {
    $containerBuilder->registerForAutoconfiguration(EventNewsDataHandlerHook::class)->addTag('eventnewsplugin.EventNewsDataHandlerHook');
    $containerBuilder->registerForAutoconfiguration(ZZZFlexFormHook::class)->addTag('eventnewsplugin.FlexFormHook');
    $containerBuilder->registerForAutoconfiguration(OverrideSettings::class)->addTag('eventnewsplugin.OverrideSettings');
//    $containerBuilder->registerForAutoconfiguration(PageLayoutView::class)->addTag('eventnewsplugin.PageLayoutView');

    $containerBuilder->addCompilerPass(new DependencyInjection\SingletonPass('eventnewsplugin.EventNewsDataHandlerHook'));
    $containerBuilder->addCompilerPass(new DependencyInjection\SingletonPass('eventnewsplugin.FlexFormHook'));
    $containerBuilder->addCompilerPass(new DependencyInjection\SingletonPass('eventnewsplugin.OverrideSettings'));
//    $containerBuilder->addCompilerPass(new DependencyInjection\SingletonPass('eventnewsplugin.PageLayoutView'));
};
