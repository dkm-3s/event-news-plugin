<?php

########################################################################
# Extension Manager/Repository config file for ext "eventnewsplugin".
########################################################################

$EM_CONF[$_EXTKEY] = [
	'title' => 'Plugin for Event News',
	'description' => 'Frontend Plugin for Event News',
	'category' => 'be',
	'author' => 'Stig Nørgaard Færch',
	'author_email' => 'snf(at)dkm.dk',
	'state' => 'beta',
	'author_company' => 'DKM',
	'version' => '2.0.0',
	'constraints' => [
		'depends' => [
			'news' => '',
			'eventnews' => '',
        ],
		'conflicts' => [
        ],
		'suggests' => [
        ],
    ],
];
